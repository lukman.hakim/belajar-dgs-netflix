| Label                         | Value 
| -----                         | ---- 
| GraphQL Explorer Local Url    | http://localhost:8080/graphiql 
| GraphQL Explorer Heroku Url   | http://belajar-dgs-netflix.herokuapp.com/graphiql 
---
# Query

### List by filter
Request Body Example
``` graphql
{
    #filter & sort using rsql
    contacts(filter:"details.value==^*keyword*", sort:"fullName,desc") { 
        id
        fullName
        organization
        details{
            id
            label
            type #Enum
            value
        }
    }
}
```

# Mutation

### Save
Request Body Example
``` graphql
mutation {
    save(request: {
        fullName: "Lukman Hakim"
        organization: "PT. ArtiVisi Intermedia"
        details: [
            {
                type: PHONE #Enum
                value: "08989899307"
                label: "Work"
            }
            {
                type: EMAIL #Enum
                value: "tjg.lukman@yopmail.com"
                label: "Work"
            }
            {
                type: ADDRESS #Enum
                value: "Asrama BS"
                label: "Home"
            }
        ]
    })
    {
        id
        fullName
        organization
        details{
            id
            label
            type #Enum
            value
        }
    }
}
```
### Delete
Request Body Example
```graphql
mutation {
    delete(id: "uuid")
}
```
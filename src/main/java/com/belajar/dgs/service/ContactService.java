package com.belajar.dgs.service;

import com.belajar.dgs.entity.EContact;
import com.belajar.dgs.generated.types.Contact;
import com.belajar.dgs.generated.types.ContactRequest;
import com.belajar.dgs.mapper.ContactMapper;
import com.belajar.dgs.repository.ContactDetailRepository;
import com.belajar.dgs.repository.ContactRepository;
import com.netflix.graphql.dgs.DgsComponent;
import com.netflix.graphql.dgs.DgsMutation;
import com.netflix.graphql.dgs.DgsQuery;
import com.netflix.graphql.dgs.InputArgument;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.jpa.domain.Specification;

import javax.transaction.Transactional;
import java.util.List;

import static io.github.perplexhub.rsql.RSQLJPASupport.toSort;
import static io.github.perplexhub.rsql.RSQLJPASupport.toSpecification;

@DgsComponent
@RequiredArgsConstructor
public class ContactService {

    private final ContactRepository contactRepo;
    private final ContactDetailRepository detailRepo;
    private final ContactMapper mapper;

    @DgsQuery
    public List<Contact> contacts(@InputArgument String filter, @InputArgument String sort) {
        Specification<EContact> spec = null;

        if (StringUtils.isNotBlank(filter))
            spec = toSpecification(filter);

        if (StringUtils.isNotBlank(sort))
            spec = spec == null ? toSort(sort) : spec.and(toSort(sort));

        return mapper.toDto(spec == null ? contactRepo.findAll() : contactRepo.findAll(spec));
    }

    @DgsMutation
    public Contact save(@InputArgument ContactRequest request){
        EContact entity = contactRepo.save(mapper.toEntity(request));
        entity.getDetails().forEach(d -> {
            d.setContact(entity);
            detailRepo.save(d);
        });

        return mapper.toDto(entity);
    }

    @DgsMutation
    @Transactional
    public Boolean delete(@InputArgument String id){
        if (!contactRepo.existsById(id)) return false;
        detailRepo.deleteAllByContactId(id);
        contactRepo.deleteById(id);
        return true;
    }

}

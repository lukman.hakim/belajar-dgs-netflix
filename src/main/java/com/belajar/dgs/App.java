package com.belajar.dgs;

import com.belajar.dgs.entity.EContact;
import com.belajar.dgs.entity.EContactDetail;
import com.belajar.dgs.generated.types.Type;
import com.belajar.dgs.repository.ContactDetailRepository;
import com.belajar.dgs.repository.ContactRepository;
import com.github.javafaker.Faker;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class App {

    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }

    @Bean
    ApplicationRunner injectData(ContactRepository contactRepo, ContactDetailRepository detailRepo){
        return args -> {
            Faker faker = new Faker();

            int i = 0;
            while (i < 10) {
                EContact contact = contactRepo.save(EContact.builder()
                        .fullName(faker.name().fullName())
                        .organization(faker.company().name())
                        .build());

                detailRepo.save(EContactDetail.builder()
                        .contact(contact)
                        .label("Work")
                        .type(Type.PHONE)
                        .value(faker.phoneNumber().cellPhone())
                        .build());

                detailRepo.save(EContactDetail.builder()
                        .contact(contact)
                        .label("Work")
                        .type(Type.EMAIL)
                        .value(faker.internet().emailAddress())
                        .build());

                detailRepo.save(EContactDetail.builder()
                        .contact(contact)
                        .label("Home")
                        .type(Type.ADDRESS)
                        .value(faker.address().fullAddress())
                        .build());

                i++;
            }

        };
    }

}

package com.belajar.dgs.mapper;

import com.belajar.dgs.entity.EContact;
import com.belajar.dgs.generated.types.Contact;
import com.belajar.dgs.generated.types.ContactRequest;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = ContactDetailMapper.class)
public interface ContactMapper extends BaseMapper<EContact, Contact, ContactRequest>{
}

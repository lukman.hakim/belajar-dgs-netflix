package com.belajar.dgs.mapper;

import java.util.List;

public interface BaseMapper <E, D, R>{
    D toDto(E e);
    List<D> toDto(List<E> es);
    E toEntity(R r);
}

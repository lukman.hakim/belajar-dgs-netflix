package com.belajar.dgs.mapper;


import com.belajar.dgs.entity.EContactDetail;
import com.belajar.dgs.generated.types.ContactDetail;
import com.belajar.dgs.generated.types.ContactDetailRequest;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ContactDetailMapper extends BaseMapper<EContactDetail, ContactDetail, ContactDetailRequest>{
}

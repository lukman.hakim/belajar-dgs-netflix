package com.belajar.dgs.repository;

import com.belajar.dgs.entity.EContact;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface ContactRepository extends JpaRepository<EContact, String>, JpaSpecificationExecutor<EContact> {
}

package com.belajar.dgs.repository;

import com.belajar.dgs.entity.EContactDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface ContactDetailRepository extends JpaRepository<EContactDetail, String>, JpaSpecificationExecutor<EContactDetail> {
    void deleteAllByContactId(String contactId);
}